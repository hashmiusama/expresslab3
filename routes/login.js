'use strict';

let express = require('express');
let login_router = express.Router();
let pug = require('pug');
let url = require('url');
let users = require('../models/users');

login_router.get('/login.html',(req, res)=>{
    //res.send(pug.renderFile('./views/static/login.html'));
    res.send(pug.renderFile('./views/login.pug', {failure: false}));
});

login_router.get('/login', (req, res)=>{
    console.log('LOGIN GET');
    if(req.session.name && req.session.from !== '/existing/yes'){
        delete users[req.session.name];
        req.session.from = 'login';
        res.send(pug.renderFile("./views/welcome.pug",
            {user: req.session.name}));
    }else{
        console.log('login to langing.');
        res.redirect('/landing');
    }
});


login_router.post('/login', (req, res)=>{
    console.log('login called.');
    if(req.session && (url.parse(req.headers.referer).pathname === '/login.html'
            || url.parse(req.headers.referer).pathname === '/login')){
        if(req.session.name === undefined){
            if(req.body.name === req.body.pwd){
                req.session.name = req.body.name;
                if(users[req.session.name] !== undefined){
                    res.send('Do you want to continue from where you left off previously? ' +
                        'Click <a href="/existing/yes">here</a>' +
                        'If you want to start a new purchase, ' +
                        'Click <a href="/existing/no">here</a>');
                }else{
                    req.session.from = 'login';
                    res.send(pug.renderFile("./views/welcome.pug", {user: req.session.name}));
                }
            }else{
                res.send(pug.renderFile('./views/login.pug', {failure: true}));
            }
        }else{
            console.log(req.session);
            req.session.from = 'login';
            res.send(pug.renderFile("./views/welcome.pug",
                {user: req.session.name}));
        }
    }else{
        console.log(req.session.from);
        if(req.session && req.session.from !== '/existing/yes'){
            res.send(pug.renderFile("./views/welcome.pug",
                {user: req.session.name}));
        }else{
            console.log('login to langing.');
            res.redirect('/landing');
        }
    }
});

module.exports = login_router;