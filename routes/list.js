'use strict';

let express = require('express');
let pug = require('pug');
let books = require('../models/books');
let url = require('url');

let list_router = express.Router();

list_router.get('/',(req, res)=>{
    req.session.current = '/list';
    console.log(req.session);
    if(req.session && (['login','list','purchase'].includes(req.session.from)
            ||(req.headers.referer) && url.parse(req.headers.referer).pathname === '/confirm' )){
        req.session.from = 'list';
        res.send(pug.renderFile('./views/list.pug', {books: books.books, name: req.session.name}));
    }else {
        res.redirect('/landing');
    }
});

module.exports = list_router;