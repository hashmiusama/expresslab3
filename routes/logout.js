'use strict';

let express = require('express');
let pug = require('pug');
let books = require('../models/books');
let users = require('../models/users');

let logout_router = express.Router();

logout_router.get('/',(req, res)=>{
    if(req.session !== undefined){
        users[req.session.name] = req.session;
    }
    req.session.destroy();
    res.redirect('/landing');
});

module.exports = logout_router;