'use strict';

let existing_router = require('express').Router();
let users = require('../models/users');

existing_router.get('/:response', (req, res)=>{
    console.log(req.params.response);
    if(req.params.response === 'yes'){
        console.log('existing yes \t' + req.session.current);
        req.session.from = users[req.session.name].from;
        req.session.current = users[req.session.name].current;
        req.session.book_cart = users[req.session.name].book_cart || [];
        req.session.session = users[req.session.name];
        req.session.card = users[req.session.name].card;
        req.session.body = users[req.session.name].body;

        res.redirect(req.session.current);
    }else{
        console.log('existing else');
        req.session.from = '/existing/no';
        res.redirect('/login');
    }
});

module.exports = existing_router;