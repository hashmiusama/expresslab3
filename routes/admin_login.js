'use strict';

let express = require('express');
let pug = require('pug');
let admins = require('../models/admins');

let admin_login_router = express.Router();

admin_login_router.get('/',(req, res)=>{
    if(req.session.admin !==  undefined){
        res.send(req.session.admin);
    }else
        res.send(pug.renderFile('./views/admin_login.pug', {failure:false}));
});

admin_login_router.post('/',(req, res)=>{
    let admin = admins[req.body.name];
    if(admin !== undefined){
        if(req.body.pwd === admin.pwd){
            req.session.admin = admin;
            res.redirect('manage');
        }else{
            res.send(pug.renderFile('./views/admin_login.pug', {failure:true}));
        }
    }else{
        res.send(pug.renderFile('./views/admin_login.pug', {failure:true}));
    }
});


module.exports = admin_login_router;